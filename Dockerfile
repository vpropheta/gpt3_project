# Use a imagem oficial do Python como base
FROM python:3.9

# Defina o diretório de trabalho no container
WORKDIR /app

# Copie o arquivo requirements.txt para o container
COPY gpt3_project/requirements.txt .

# Instale as dependências do projeto
RUN pip install -r requirements.txt

# Copie o diretório do projeto para o container
COPY gpt3_project .

# Execute as migrações e configure o banco de dados
RUN python manage.py migrate

# Exponha a porta onde o aplicativo estará em execução
EXPOSE 8000

# Inicie o aplicativo Django
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]