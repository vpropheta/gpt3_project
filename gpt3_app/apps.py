from django.apps import AppConfig


class Gpt3AppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'gpt3_app'
