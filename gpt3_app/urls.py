from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('generate-response/', views.generate_response, name='generate_response'),
    path('get-response/<int:response_id>/', views.get_response, name='get_response'),
]