import openai
import torch
from transformers import BertTokenizer, BertModel
from django.http import JsonResponse
from .models import Prompt, Context, GeneratedResponse, VectorStore
from django.shortcuts import render

# Carregue o modelo BERT e o tokenizador
bert_model = BertModel.from_pretrained("bert-base-uncased")
tokenizer = BertTokenizer.from_pretrained("bert-base-uncased")

vector_store = VectorStore(vector_dim=768)

def home(request):
    return render(request, 'generate_response.html') 

def generate_response(request):
    user_prompt = request.GET.get('prompt')
    user_context = request.GET.get('context')

    # Dica: Use um prompt descritivo para fornecer instruções claras
    model_prompt = f"Gere um resumo sobre o seguinte tópico:\n{user_prompt}"

    # Dica: Adicione contexto relevante no prompt
    if user_context:
        model_prompt += f"\nContexto: {user_context}"

    # Configure sua chave de API GPT-3.5 aqui
    openai.api_key = '.....'

    # Dica: Ajuste os parâmetros de temperatura e máx. tokens conforme necessário
    response = openai.Completion.create(
        engine="text-davinci-002",
        prompt=model_prompt,
        max_tokens=100,  # Ajuste o tamanho máximo da resposta
        temperature=0.7  # Ajuste a temperatura para controlar a criatividade
    )

    generated_text = response.choices[0].text

    # Tokenize e embeddings da resposta usando BERT
    input_ids = tokenizer.encode(generated_text, add_special_tokens=True, max_length=512, truncation=True, padding='max_length')
    input_ids = torch.tensor(input_ids).unsqueeze(0)
    with torch.no_grad():
        bert_outputs = bert_model(input_ids)
    
    # Extrair embeddings da última camada oculta do BERT
    embeddings = bert_outputs.last_hidden_state.mean(dim=1).numpy()  # Usando a média das embeddings de todas as palavras

    # Armazene a resposta no Vector Store
    response_id = 1  # Substitua isso pela maneira como você gera IDs únicos para respostas
    vector_store.insert_vector(response_id, embeddings[0])

    # Armazene a resposta no banco de dados
    prompt_obj, _ = Prompt.objects.get_or_create(text=user_prompt)
    context_obj, _ = Context.objects.get_or_create(text=user_context)
    GeneratedResponse.objects.create(prompt=prompt_obj, context=context_obj, response=generated_text, vector_id=response_id)

    return JsonResponse({'response': generated_text})

def get_response(request, response_id):
    response = GeneratedResponse.objects.get(pk=response_id)
    vector = vector_store.get_vector(response.vector_id)

    # Recupere a representação vetorial da resposta usando o Vector Store
    if vector is not None:
        distances, indices = vector_store.search_vectors(vector, k=1)
        return JsonResponse({'response': response.response, 'distance': distances[0], 'index': indices[0]})
    else:
        return JsonResponse({'response': response.response, 'distance': None, 'index': None})