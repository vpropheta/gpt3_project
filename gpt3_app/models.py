import faiss
import numpy as np
from django.db import models

class VectorStore:
    def __init__(self, vector_dim):
        self.vector_dim = vector_dim
        self.index = faiss.IndexFlatL2(vector_dim)
        self.vector_mapping = {}

    def insert_vector(self, response_id, vector):
        if vector.shape[0] != self.vector_dim:
            raise ValueError("Vector dimension mismatch.")
        self.index.add(np.expand_dims(vector, axis=0))
        self.vector_mapping[response_id] = vector

    def search_vectors(self, query_vector, k=1):
        if query_vector.shape[0] != self.vector_dim:
            raise ValueError("Query vector dimension mismatch.")
        distances, indices = self.index.search(np.expand_dims(query_vector, axis=0), k)
        return distances, indices

    def get_vector(self, response_id):
        return self.vector_mapping.get(response_id, None)  # Retorna None se o ID não existir

class Prompt(models.Model):
    text = models.TextField()

class Context(models.Model):
    text = models.TextField()

class GeneratedResponse(models.Model):
    prompt = models.ForeignKey(Prompt, on_delete=models.CASCADE)
    context = models.ForeignKey(Context, on_delete=models.CASCADE)
    response = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    vector_id = models.IntegerField()  # Campo para armazenar o ID do vetor correspondente

    def __str__(self):
        return f"Response for Prompt: {self.prompt.text}"