# GPT-3.5 Django Integration with Vector Store

This is a project that integrates the GPT-3.5 model with a Vector Store in a Django application. The application allows you to generate responses with GPT-3.5 based on provided prompts and contexts and store the vector representations of the responses in a Vector Store. Additionally, you can retrieve responses based on vector queries from the Vector Store.
Environment Setup

## Before running the project, make sure you have the following dependencies installed:

- Python 3.x
- Django
- OpenAI GPT-3.5 API
- Hugging Face Transformers Library
- Faiss (Vector search library)

You will also need a valid GPT-3.5 API key to send requests to the model.

## Application Setup

1. Clone this repository to your local machine.

2. Create a virtual environment (optional but recommended):

   python -m venv venv
   source venv/bin/activate  # No Windows, use 'venv\Scripts\activate'

3. Install the dependencies:

    pip install -r requirements.txt

4. Configure your GPT-3.5 API key in the generate_response function within the views.py file:

    openai.api_key = 'sua-chave-de-api'

5. Run database migrations:

    python manage.py makemigrations
    python manage.py migrate

6. Start the Django server:

    python manage.py runserver

7.     Access the application at http://localhost:8000/.

## Using the Application

    Access the homepage at http://localhost:8000/.

    Fill in the "Prompt" field with the desired prompt text and the "Context" field with relevant context.

    Click the "Generate Response" button to generate a response using GPT-3.5.

    The generated response will be displayed in the "Generated Response" section.

    Vector representations of the responses are stored in the Vector Store and can be retrieved based on vector queries.

## Retrieving Responses via Vector Queries

You can retrieve responses based on vector queries using the following route:

    /get-response/<int:response_id>/

Replace <int:response_id> with the ID of the response you want to retrieve. The response will be returned along with the distance and corresponding index in the Vector Store.

## Project Structure

    urls.py: Defines the application's URLs, including routes for the homepage, response generation, and response retrieval.

    views.py: Contains the application's views, including the logic for generating responses, storing them in a Vector Store, and retrieving responses.

    models.py: Defines the data models, including the Vector Store, generated responses, prompts, and associated contexts.

    generate_response.html: An HTML template for the homepage that allows users to input prompts and contexts and displays the generated responses.